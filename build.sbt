enablePlugins(ScalaJSPlugin, WorkbenchPlugin)

name := "Operations"

version := "0.1-SNAPSHOT"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.scala-js" %%% "scalajs-dom" % "0.9.1",
  "com.lihaoyi" %%% "scalatags" % "0.5.4",
  "com.github.karasiq" %%% "scalajs-bootstrap" % "2.2.1",
  "com.lihaoyi" %%% "upickle" % "0.4.1",
  "com.github.japgolly.scalacss" %%% "ext-scalatags" % "0.5.3",
  "org.scalatest" %% "scalatest" % "3.0.4" % "test"
)