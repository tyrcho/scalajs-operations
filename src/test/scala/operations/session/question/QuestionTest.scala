package operations.session.question

import org.scalatest.{FlatSpec, Matchers}

class QuestionTest extends FlatSpec with Matchers {
  "Question" should "have score -1 when all wrong" in {
    Question("q", "a", 0, 10).score shouldBe -1
  }

  "Question" should "have score 1 when all OK" in {
    Question("q", "a", 10, 0).score shouldBe 1
  }

  "Question" should "have score 0.5 when same number OK and KO" in {
    Question("q", "a", 5, 5).score shouldBe 0.5
  }
}
