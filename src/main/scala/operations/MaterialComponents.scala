package operations

import org.scalajs.dom.html.{Button, Div, Input, Label}

object MaterialComponents {

  import scalatags.JsDom.all._

  def divInputNumber(i: String="",
                     default: String = "?",
                     error: String = "Not a number",
                     min: Option[Int] = None,
                     max: Option[Int] = None): (Div, Input) = {
    val in = input(cls := "mdl-textfield__input",
      tpe := "number",
      id := i,
      value := default,
      pattern := "-?[0-9]+").render
    min.foreach(i => in.min = i.toString)
    max.foreach(i => in.max = i.toString)
    val d = div(cls := "mdl-textfield mdl-js-textfield",
      in,
      maxWidth := 2.em,
      label(cls := "mdl-textfield__label", `for` := i, default),
      span(cls := "mdl-textfield__error", error)
    ).render
    (d, in)
  }

  def startButton: Button = fabButton("play_arrow", "mdl-button--primary")

  def stopButton: Button = fabButton("stop", "mdl-button--accent")

  def fabButton(name: String, extraClasses: String*): Button = {
    val classes = extraClasses.mkString("mdl-button mdl-button--fab ", " ", "")
    button(
      cls := classes,
      i(cls := "material-icons", name)
    ).render
  }

  def labelCheckbox(i: String, text: String): (Label, Input) = {
    val cb = input(cls := "mdl-checkbox__input",
      tpe := "checkbox",
      id := i).render
    val lab = label(cls := "mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect", `for` := i,
      cb,
      span(cls := "mdl-checkbox__label", text)
    ).render
    (lab, cb)
  }

  def labelSwitch(i: String, text: String): (Label, Input) = {
    val cb = input(cls := "mdl-switch__input",
      tpe := "checkbox",
      id := i).render
    val lab = label(cls := "mdl-switch mdl-js-switch mdl-js-ripple-effect", `for` := i,
      cb,
      span(cls := "mdl-switch__label", text)
    ).render
    (lab, cb)
  }
}
