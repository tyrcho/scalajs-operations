package operations.session.question

import operations.AddableStyle
import operations.OperationsStyle._
import org.scalajs.dom.ext.KeyValue
import org.scalajs.dom.html._
import org.scalajs.dom.raw.KeyboardEvent

import scalatags.JsDom.all._
import scalacss.DevDefaults._
import scalacss.ScalatagsCss._

object QuestionView {

  import QuestionStyle._

  private val questionSpan: Span = span(
    mainText, question,
    "question"
  ).render

  private val box =
    input(
    tpe := "number",
    autofocus := true,
    mainInput,
    placeholder := "?").render

  val render: Div =
    div(flexBoxWrap, divWithBorder,
      questionSpan,
      box
    ).render

  def display(question: String): Unit = {
    questionSpan.innerHTML = question
    box.focus()
  }

  def onValidate(f: String => Unit): Unit =
    box.onkeypress = (e: KeyboardEvent) =>
      if (KeyValue.Enter == e.key && box.value.trim != "")
        f(box.value)

  def clear(): Unit = {
    box.value = ""
    box.focus()
  }

  def setEnabled(enabled: Boolean): Unit = {
    render.style.display = if (enabled) "" else "none"
  }
}


object QuestionStyle extends AddableStyle {

  import dsl._

  val question = style(margin(1.vmin))
  val mainInput = style(mainText, width(50.%%), maxWidth :=! "3em")

  addCss()
}
