package operations.session.question

case class Question(question: String, answer: String, goods: Int = 0, bads: Int = 0) {
  override def toString = s"$question = $answer"

  val total: Int = goods + bads

  //between -1 (all wrong) and 1 (all good). 0 = never done
  val score: Float =
    if (total == 0) 0
    else if (goods == 0) -1
    else goods.toFloat / total

  private def normalizeNumber(n: String): String =
    n.trim.dropWhile(_ == '0')

  def check(ans: String): Boolean =
    normalizeNumber(ans) == normalizeNumber(answer)
}