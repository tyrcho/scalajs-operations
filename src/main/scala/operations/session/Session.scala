package operations.session

import operations.session.question.Question

case class Session(questions: Vector[Question], pos: Int = 0, goods: Int = 0, bads: Int = 0) {
  val size: Int = questions.size
  val todo: Int = size - pos
  val progress: Int = (size - todo) * 100 / size // percentage completed

  def currentQuestion: Question = questions(pos)

  def ok(ans: String): Boolean = currentQuestion.check(ans)

  def hasNext: Boolean = pos < questions.size

  def next(ans: String): Session = copy(
    pos = pos + 1,
    goods = goods + (if (ok(ans)) 1 else 0),
    bads = bads + (if (ok(ans)) 0 else 1))

  def ratio: Float = goods.toFloat / size
}
