package operations.session


import operations.AddableStyle
import operations.OperationsStyle._
import operations.config.ConfigStore
import org.scalajs.dom.html.Div

import scala.concurrent.duration.DurationInt
import scala.scalajs.js
import scalacss.ScalatagsCss._
import scalatags.JsDom.all._

object AnswersView {

  import AnswersStyle._

  val render: Div = div(divWithBorder, answers).render

  def clear(): Unit = render.innerHTML = ""

  def addAnswer(session: Session, answer: String): Unit = {
    val ok = session.ok(answer)
    val sign = if (ok) "✓" else "✘"
    val colorOk = if (ok) "green" else "red"
    val result = div(
      s"${session.currentQuestion} ($sign)",
      color := colorOk,
      fontSize := 200.pct,
      margin := "2vmin").render
    render.insertBefore(result, render.firstChild)
    scalajs.js.timers.setTimeout(3.seconds)(result.style.fontSize = 100.pct)
    val sound = if (ok) "good" else "bad"
    playSound(sound)
  }

  private def playSound(sound: String): Unit = {
    ConfigStore.soundLevel.foreach { level =>
      val audio = js.Dynamic.newInstance(js.Dynamic.global.Audio)(s"sound/$sound.mp3")
      audio.volume = level / 100.0
      audio.play()
    }
  }
}


import scalacss.DevDefaults._

object AnswersStyle extends AddableStyle {

  import dsl._

  val answers = style(flexDirection.column, flexBoxWrap,
    media.landscape(maxHeight(40.vh)),
    media.portrait(maxHeight(60.vh))
  )

  addCss()
}

