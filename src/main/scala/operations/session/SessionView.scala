package operations.session


import operations.MaterialComponents
import operations.OperationsStyle._
import operations.session.question.QuestionView
import org.scalajs.dom.html._
import org.scalajs.dom.raw.MouseEvent

import scalacss.ScalatagsCss._
import scalatags.JsDom.all._
import scalatags.JsDom.tags2._

object SessionView {
  private val stopButton = MaterialComponents.stopButton
  stopButton.onclick = { (_: MouseEvent) => closeSession() }

  val render: Element =
    section(
      div(flexBoxWrap, divWithBorder,
        ProgressView.render,
        stopButton),
      QuestionView.render,
      AnswersView.render).render

  QuestionView.onValidate(checkAnswer)

  var endSession: (Float => Unit) = _

  def startSession(): Unit = {
    session = Session(Vector.fill(10)(Judge.build()).distinct)
    AnswersView.clear()
    setEnabled(true)
    showNextQuestion()
    ProgressView.update(session)
  }

  private def checkAnswer(answer: String): Unit
  = {
    AnswersView.addAnswer(session, answer)
    session = session.next(answer)
    ProgressView.update(session)
    showNextQuestion()
    QuestionView.clear()
  }

  private def showNextQuestion(): Unit

  =
    if (session.hasNext) QuestionView.display(session.currentQuestion.question + " = ")
    else closeSession()

  private var session: Session =
    _

  private def setEnabled(enabled: Boolean): Unit

  = {
    val display = if (enabled) "" else "none"
    stopButton.style.display = display
    QuestionView.setEnabled(enabled)
  }

  private def closeSession(): Unit

  = {
    setEnabled(false)
    endSession(session.ratio)
  }
}