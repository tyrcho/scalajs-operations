package operations.session

import operations.config.ConfigStore._
import operations.session.question.Question

import scala.util.Random

object Judge {

  def build(): Question = {
    val qs = for {
      (op, gen) <- `4_operations` zip generators
      if config(op)
      l = level(op)
      a = nextInt(l, l * 3F / 5)
      b = nextInt(l, 1)
    } yield gen(a, b)
    Random.shuffle(qs).head
  }

  private def nextInt(max: Int, min: Float) =
    (util.Random.nextFloat() * (max + 1 - min) + min).toInt

  type QuestionGenerator = (Int, Int) => Question

  private val ADD: QuestionGenerator = (a, b) => Question(s"$a + $b", s"${a + b}")
  private val MUL: QuestionGenerator = (a, b) => Question(s"$a x $b", s"${a * b}")
  private val SUB: QuestionGenerator = (a, b) => Question(s"${a + b} - $b", s"$a")
  private val DIV: QuestionGenerator = (a, b) => Question(s"${a * b} / $b", s"$a")
  private val generators = Seq(ADD, SUB, MUL, DIV)
}
