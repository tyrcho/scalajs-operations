package operations.session

import operations.OperationsStyle._
import org.scalajs.dom.html.Div

import scalacss.ScalatagsCss._
import scalatags.JsDom.all._

object ProgressView {
  val render: Div = div(flexBoxWrap).render

  def update(session: Session): Unit = {
    render.innerHTML = ""
    render.appendChild(
      div(mainText,
        margin := "2vmin",
        span(color := "green", session.goods + "✓"),
        span(" "),
        span(color := "red", session.bads + "✘")).render)
    render.appendChild(
      div(
        flex := 1,
        minWidth := "50vw",
        height := "5vh",
        marginTop := "1vh",
        marginBottom := "1vh",
        backgroundColor := "#cacaca",
        div(
          backgroundColor := "#4CAF50",
          height := "100%",
          width := session.progress.pct
        )
      ).render
    )
  }
}
