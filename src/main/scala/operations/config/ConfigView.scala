package operations.config

import operations.OperationsStyle._
import operations.config.ConfigStore._
import operations.{AddableStyle, MaterialComponents}
import org.scalajs.dom.html.{Div, Input}
import org.scalajs.dom.raw.{Event, MouseEvent}

import scala.scalajs.js
import scala.scalajs.js.Any._
import scalacss.ScalatagsCss._
import scalatags.JsDom.all._

object ConfigView {

  val render: Div =
    div(
      divWithBorder,
      flexBoxWrap,
      div(flexBoxWrap,
        `4_operations`.map(configCB),
        configCB("sound")
      ),
      startButton
    ).render

  def onStart(f: () => Unit): Unit =
    startButton.onclick = (_: MouseEvent) => f()

  def disable(): Unit = render.style.display = "none"

  def focus(): Unit = {
    render.style.display = ""
    startButton.focus()
  }

  def update(ratio: Float): Unit = {
    for {
      (operationName, active) <- config
      if active
      lvlInput = js.Dynamic.global.document.getElementById(operationId(operationName))
    } {
      println(s"ratio=$ratio")
      if (ratio >= 0.9) lvlInput.stepUp()
      else if (ratio < 0.8) lvlInput.stepDown()
      ConfigStore.updateLevel(operationName, lvlInput.value.asInstanceOf[String].toInt)
    }
  }

  private lazy val startButton =
    MaterialComponents.startButton

  private def configCB(operationName: String) = {
    val (lb, cb) = MaterialComponents.labelSwitch(operationName, operationName)
    cb.onchange = { (e: Event) => ConfigStore.updateActive(operationName, e.target.asInstanceOf[Input].checked) }
    if (config(operationName)) cb.checked = true

    val (d, i) = MaterialComponents.divInputNumber(
      operationId(operationName),
      ConfigStore.level(operationName).toString,
      min = Some(0))
    i.onchange = { (e: Event) => ConfigStore.updateLevel(operationName, e.target.asInstanceOf[Input].value.toInt) }

    div(lb, d, divWithBorder).render
  }

  private def operationId(label: String) = s"$label level"

}

import scalacss.DevDefaults._

object ConfigStyle extends AddableStyle {

  import dsl._

  addCss()
}
