package operations.config

import org.scalajs.dom.ext.LocalStorage
import upickle.default.{read, write}

object ConfigStore {
  val `4_operations` = Seq("+", "-", "x", "/")

  private val defaultActive = Map("+" -> true)
  private val defaultLevels = `4_operations`.map(_ -> 2).toMap

  def config: Config =
    LocalStorage("config")
      .map(read[Config])
      .getOrElse(defaultActive)
      .withDefaultValue(false)

  def level: Level =
    LocalStorage("levels")
      .map(read[Level])
      .getOrElse(defaultLevels)
      .withDefaultValue(5)

  def soundLevel: Option[Int] =
    if (config("sound")) Some(level("sound") min 100)
    else None

  def updateLevel(operationName: String, newLevel: Int): Unit =
    writeLevel(level.updated(operationName, newLevel))

  def updateActive(operationName: String, active: Boolean): Unit =
    writeActive(config.updated(operationName, active))

  private def writeLevel(c: Level): Unit =
    LocalStorage.update("levels", write(c))

  private def writeActive(c: Config): Unit =
    LocalStorage.update("config", write(c))

  type Config = Map[String, Boolean]
  type Level = Map[String, Int]
}
