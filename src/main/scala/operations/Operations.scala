package operations

import operations.config.ConfigView
import operations.session.SessionView
import org.scalajs.dom.html.Button

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}
import scalacss.DevDefaults._

@JSExportTopLevel("Operations")
object Operations {

  private val document = js.Dynamic.global.document
  private val root = document.body

  private def updateConfig(finalRatio: Float): Unit = {
    ConfigView.update(finalRatio)
    ConfigView.focus()
  }

  @JSExport
  def main(env: String): Unit = {
    sys.props.update("env", env)

    root.appendChild(ConfigView.render)
    ConfigView.onStart(startSession)
    ConfigView.focus()
    SessionView.endSession = updateConfig
  }

  def startSession() {
    ConfigView.disable()
    root.appendChild(SessionView.render)
    SessionView.startSession()
  }
}


trait AddableStyle extends StyleSheet.Inline {
  def addCss(): Unit = {
    val env = sys.props("env")
    println(s"adding style for ${this.getClass.getSimpleName} in env $env")
    if (env == "dev") {
      val CssSettings = scalacss.DevDefaults
      import CssSettings._
      this.addToDocument()
    } else {
      val CssSettings = scalacss.ProdDefaults
      import CssSettings._
      this.addToDocument()
    }
  }
}


object OperationsStyle extends AddableStyle {

  import dsl._

  style(
    unsafeRoot("*")(
      lineHeight.unset, // MDL fixes to 20px
      boxSizing.borderBox,
      // See https://madebymike.com.au/writing/precise-control-responsive-typography/
      media.minWidth(800.px).not("i")(fontSize(4.vmin).important),
      //       at width 800px => 32px
      media.maxWidth(800.px).not("i")(fontSize :=! "calc(16px + 2vmin) !important")),
    unsafeRoot("body")(
      padding(1.vw, 1.vw, 1.vh, 1.vh),
      backgroundImage :=
        """ linear-gradient(transparent 11px, rgba(220,220,200,.8) 12px, transparent 12px),
            linear-gradient(90deg, transparent 11px, rgba(220,220,200,.8) 12px, transparent 12px)""",
      backgroundSize := "100% 12px, 12px 100%"),
    unsafeRoot("input[type='checkbox']")(margin(0.5.em))
  )

  val mainText = style(fontSize(200.%%))
  val flexBoxWrap = style(display.flex, flexWrap.wrap)

  val divWithBorder = style(
    border(black), borderStyle(solid), borderWidth(1.px), borderRadius(1.vmin),
    padding(1.vw, 1.vw, 1.vh, 1.vh),
    margin(1.vw, 1.vw, 1.vh, 1.vh),
    background := "white",
    marginBottom(1.vh))
  addCss()
}
